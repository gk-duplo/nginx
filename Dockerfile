FROM nginx:latest
COPY ./index.html  /usr/share/nginx/html/
RUN sed -ie "s/DATE/$(date)/g" /usr/share/nginx/html/index.html