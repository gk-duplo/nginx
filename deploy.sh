#!/bin/bash
echo "Initiating deployment: Duplo Host: $DUPLO_HOST, Duplo Tenant: $DUPLO_TENANT_ID, Git Commit $GIT_COMMIT"
curl -Ssf -H 'Content-type: application/json' \
            -H "Authorization: Bearer $DUPLO_TOKEN" -XPOST \
            "${DUPLO_HOST}/subscriptions/$DUPLO_TENANT_ID/ReplicationControllerChange" \
            -d '{
              "Name": "nlp",
              "AllocationTags": "nlp",
              "Image": "gkhakare/nginx:${GIT_COMMIT}",
              "AgentPlatform": "0"
            }'